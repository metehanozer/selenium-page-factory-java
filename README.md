# 🎁 selenium-page-factory-java
Java Selenium webDriver test project with page factory

### Test
~~~
./mvnw clean test -Dsuite=testng-local
./mvnw clean test -Dsuite=testng-local -Dbrowser=firefox
./mvnw clean test -Dsuite=testng-local -DthreadCount=1 -Dbrowser=firefox
~~~