package page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

    private static String pageUrl = "https://www.imdb.com/";

    @FindBy(id = "imdbHeader-navDrawerOpen--desktop")
    private WebElement mainMenuLabel;

    @FindBy(linkText = "Top Rated Movies")
    private WebElement topRatedMoviesSpan;

    @FindBy(xpath = "//input[@id='suggestion-search']")
    private WebElement searchInput;

    @FindBy(xpath = "//button[@id='suggestion-search-button']")
    private WebElement searchButton;

    public HomePage(WebDriver webDriver) {
        webDriver.get(pageUrl);
        PageFactory.initElements(webDriver, this);
    }

    public void searhTo(String keyword){
        searchInput.clear();
        searchInput.sendKeys(keyword);
        searchButton.click();
    }

    public void searchInputSendKeys(String keyword){
        searchInput.clear();
        searchInput.sendKeys(keyword);
    }

    public void searchButtonClick(){
        searchButton.click();
    }

    public void openTop250MoviePage(){
        mainMenuLabel.click();
        topRatedMoviesSpan.click();
    }
}
