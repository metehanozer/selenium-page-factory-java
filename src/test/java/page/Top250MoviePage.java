package page;

import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Top250MoviePage {

    @FindBy(css = ".recently-viewed")
    private WebElement bottomElement;

    @FindAll(@FindBy(xpath = "//tbody/tr/td[2]"))
    private List<WebElement> top250MovieList;

    public Top250MoviePage(WebDriver webDriver) {
        PageFactory.initElements(webDriver, this);
    }

    public boolean bottomElementIsDisplayed() {
        return bottomElement.isDisplayed();
    }

    public int top250MovieListSize() {
        System.out.println("Top 250 movie size: " + top250MovieList.size());
        return top250MovieList.size();
    }
}
