package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import page.Top250MoviePage;
import utils.TestUtil;

public class Top250MovieTest2 extends TestUtil {

    @Test
    public void checkTop250Movie21() {
        System.out.println("Test started for thread id: " +  Thread.currentThread().getId());
        System.out.println("Test started for thread id: " +  Thread.currentThread().getId());

        homePage.openTop250MoviePage();
        Top250MoviePage top250Page = new Top250MoviePage(webDriver);
        Assert.assertTrue(top250Page.bottomElementIsDisplayed());
        Assert.assertEquals(top250Page.top250MovieListSize(), 250, "Top 250 movie size error!");
    }

    @Test
    public void checkTop250Movie22() {
        System.out.println("Test started for thread id: " +  Thread.currentThread().getId());

        homePage.openTop250MoviePage();
        Top250MoviePage top250Page = new Top250MoviePage(webDriver);
        Assert.assertTrue(top250Page.bottomElementIsDisplayed());
        Assert.assertEquals(top250Page.top250MovieListSize(), 250, "Top 250 movie size error!");
    }
}
