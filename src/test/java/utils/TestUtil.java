package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import page.HomePage;

public class TestUtil {

    public WebDriver webDriver;
    protected HomePage homePage;

    @BeforeClass
    @Parameters({"browser"})
    public void setupDriver(@Optional("chrome") String browser) throws MalformedURLException {

        if (browser.equalsIgnoreCase("chrome")) {
            System.setProperty("webdriver.chrome.driver", System.getenv("CHROME_DRIVER"));
            webDriver = new ChromeDriver();
            webDriver.manage().window().maximize();

        } else if (browser.equalsIgnoreCase("firefox")) {
            System.setProperty("webdriver.gecko.driver", System.getenv("GECKO_DRIVER"));
            webDriver = new FirefoxDriver();
            webDriver.manage().window().maximize();

        } else {
            System.out.println("Vatdıfakisdet!");
            System.exit(1);
        }
        webDriver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }

    @BeforeMethod(alwaysRun = true)
    public void homePage(){
        homePage = new HomePage(webDriver);
    }

    @AfterClass
    public void quitDriver() {
        webDriver.quit();
    }
}